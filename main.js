// It is always helpful to use comments in your code!
var main = document.getElementsByClassName('main-menu');
var sub = document.getElementsByClassName('sub-menu');
var label = document.getElementsByClassName('main-menu-labels');
var tog = document.getElementsByClassName('tog');


console.log(main[0].children);
console.log(tog[0].children);
console.log(sub[0].children);
console.log(label[0].children)


function toggleDropdown(){
   // sub[0].classList.toggle('condensed');
   // sub[1].classList.toggle('condensed');
   // sub[2].classList.toggle('condensed');

    this.children[1].classList.toggle('condensed');
    console.log(this)
};

console.log('this');


for (i=0;i<tog.length;i++){
    tog[i].addEventListener('mouseenter', toggleDropdown);
    tog[i].addEventListener('mouseleave', toggleDropdown);
}


